import axios from "axios";

const appAxios  = axios.create({
    baseURL: '/',
    timeout: 10000,
});

export default appAxios;