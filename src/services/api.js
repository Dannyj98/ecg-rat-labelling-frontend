import Axios from "axios";

export const getInitialGraphData = (async () => {
    try{
        const response = await appAxios.get("assets/data.json");
        return response.data;
    }
    catch(err){
        console.log('getInitialGraphData: Fetch unsuccessful', err);
        throw err;
    }


export const uploadEcgData = (async (file) => {
    try{
        let formData = new FormData();
        formData.append('file', file)
        const response = await appAxios.post("http://127.0.0.1:5000/upload-ecg", formData, {timeout: 100000});
        console.log(JSON.parse(response.data.ecg_data))
        return response.data;
    }
    catch(err){
        console.log('uploadEcgData: Post unsuccessful', err);
        throw err;
    }
})

// export const getInitialGraphData = (async () => {
//     try{
//         const response = await appAxios.get("assets/data.json");
//         return response.data;
//         // console.log(response);
//     }
//     catch(err){
//         console.log('getInitialGraphData: Fetch unsuccessful', err);
//         throw err;
//     }

// });

// export const getEcgFiles = ( () => {
//     try {
//         const response = appAxios.get("https://v710zlh4ec.execute-api.eu-west-2.amazonaws.com/dev", {
//             headers: {
//                 'Accept':'application/json',
//                 'Content-Type':'application/json'
//             }, crossDomain: true
//         }).then(({ data }) => data)
//         .catch(console.log())
//         console.log('response:', response);
//         return response.data;
//     }
//     catch(err) {
//         console.log('getEcgFiles: Fetch unsuccessful', err);
//         throw err;
//     }
// })

// export const uploadEcgData = (async (file) => {
//     try{
//         // const response = await appAxios.post("/", file);
//         console.log(file);
//         const response = await Promise.resolve({data: []});
//         return response.data;
//     }
//     catch(err){
//         console.log('uploadEcgData: Fetch unsuccessful', err);
//         throw err;
//     }
// })
