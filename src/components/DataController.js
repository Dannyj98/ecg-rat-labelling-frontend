import React, {useState} from "react";
import { Button, Form, Row, Col } from "react-bootstrap";
import "./DataController.css"


function DataController({childToParent}) {

    const [file, setFile] = useState(null);

    const handleOnChange = (e) => {
        setFile(e.target.files[0]);
    };



  return (
      <div className="DataController">
        <Form>
            <Row>
                <Col xs={8}>
                    <Form.Control type="file" onChange={handleOnChange} accept=".csv, .xlsx, .xls"/>
                </Col>
                <Col>
                <Button variant="primary" onClick={childToParent(file)}>Annotate Labels</Button>
                </Col>
            </Row>
      </Form>
      </div>
  );
}

export default DataController;
