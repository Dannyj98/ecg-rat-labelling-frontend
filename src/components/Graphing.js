import Dygraph from "dygraphs";
import React, { useState, useEffect } from "react";
import { Button, Form, Row, Col, Table, Alert } from "react-bootstrap";
import "./Graphing.css";
import { TABLE_DATA } from "./../constants/app_constants";
import { uploadEcgData } from "../services/api";
import csvDownload from 'json-to-csv-export'

function Graphing() {
  const [graphData, setGraphData] = useState([]);
  const [annotations, setAnnotations] = useState([]);
  const [file, setFile] = useState();
  const [isFilePicked, setIsFilePicked] = useState(false)

  const [showAlert, setShowAlert] = useState(false)

  const handleOnChange = async (e) => {
    const uploadedFile = e.target.files[0];
    const data = await uploadEcgData(uploadedFile)
    setGraphData(JSON.parse(data.ecg_data));
    const newAnn = JSON.parse(data.label)
    var tempAnnotations = [];
    newAnn.map((x) => tempAnnotations.push({
      series: "Raw",
      x: String(x[0]),
      tickWidth: 5,
      tickHeight: 10,
      tickColor: "#fac02b",
      isCustom: true,
    }))
    setAnnotations(newAnn)
  };

  const handleOnClick = () => {
    const csvdata = graphData.map((row, index) => {
      return {
        "Peak value": row.x,
        "Annotation by": row.isCustom ? "User" : "System",
      };
    });
    csvDownload(csvdata);
  };

  useEffect(() => {
    if (graphData.length >= 1) {
      const g = new Dygraph("graph", graphData, {
        labels: ["Time", "Raw"],
        width: 1000,
        showRangeSelector: true,
        clickCallback: function (e, x, points) {
          var tempAn = g.annotations();
          tempAn.push({
            series: "Raw",
            x: String(x),
            tickWidth: 5,
            tickHeight: 10,
            tickColor: "#fac02b",
            isCustom: true,
          });
          g.setAnnotations(tempAn);
          setAnnotations(tempAn)
        },
      });
      g.setAnnotations(annotations);
    }
  }, [graphData, annotations]);

  const removeAnnotation = (annotation) => {
    const updatedLabels = annotations.filter((val) => val.x !== annotation.x);
    setAnnotations([...updatedLabels]);
  };

  const renderAnnotationListItem = (annotation, key) => {
    return (
      <tr key={key}>
        <td>
          <span>{key + 1}</span>
        </td>
        <td>
          <span>{annotation.x}</span>
        </td>
        <td>
          <span>{annotation.isCustom ? "User" : "System"}</span>
        </td>
        <td onClick={() => removeAnnotation(annotation)}>
          <Button className="delete-link" variant="link">
            Delete
          </Button>
        </td>
      </tr>
    );
  };

  return (
    <div>
      <div className="DataController">
        <Form>
          <Row>
            <Col xs={8}>
              <Form.Control
                type="file"
                onChange={handleOnChange}
                accept=".csv, .xlsx, .xls"
              />
            </Col>
            <Col>
              <Button
                variant="success"
                disabled={graphData.length === 0}
                onClick={handleOnClick}
              >
                Download
              </Button>
            </Col>
          </Row>
        </Form>
      </div>
      <p>
        {ecgFiles}
      </p>

      <div id="graph"></div>
      {graphData.length === 0 && <div className="no-data">No data to display</div>}
      <div className="table-wrap">
        <Table className="help-table">
          <thead>
            <tr>
              {TABLE_DATA.headers.map((header, key) => (
                <th key={key}>{header}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {TABLE_DATA.body.map((t_body, key) => (
              <tr key={key}>
                <td>{t_body.action}</td>
                <td>{t_body.result}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        <div></div>
        {annotations.length > 0 && (
          <Table className="annotations-list">
            <thead>
              <tr>
                {["S.No", "Timestamp", "Annotation type", ""].map(
                  (header, key) => (
                    <th key={key}>{header}</th>
                  )
                )}
              </tr>
            </thead>
            <tbody>
              {annotations.map((_, index) =>
                renderAnnotationListItem(
                  annotations[annotations.length - (index + 1)],
                  index
                )
              )}
            </tbody>
          </Table>
        )}
      </div>
    </div>
  );
}

export default Graphing;
