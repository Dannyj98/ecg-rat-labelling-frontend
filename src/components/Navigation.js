import { Navbar, Nav, Container } from 'react-bootstrap';

function Navigation() {
  return (
    <div>
    <Navbar bg="dark" variant="dark">
      <Container>
      <Navbar.Brand href="#home">ECG-Rat Labelling</Navbar.Brand>
      <Nav className="me-auto">
        <Nav.Link href="#">Upload Model</Nav.Link>
        <Nav.Link href="#">Upload Data</Nav.Link>
        
      </Nav>
      </Container>
    </Navbar>
    </div>
  );
}

export default Navigation;
