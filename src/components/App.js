import './App.css';
import Navigation from './Navigation.js'
import Graphing from './Graphing'
import {Container} from 'react-bootstrap'

function App() {
  return (
    <div>
      <Navigation/>
      <Container>
        <Graphing />
      </Container>
    </div>
  );
}

export default App;
