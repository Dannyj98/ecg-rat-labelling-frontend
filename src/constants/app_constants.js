
export const TABLE_DATA = {
        headers: ["Action", "Result"],
        body: [
            {action: "Single Click", result: "Add Annotation"},
            {action: "Double Click", result: "Remove Annotation (Not Implemented Yet)"},
            {action: "Slide Range Finder (below graph)", result: "Zoom In/Out"},
            {action: "Move Range Finder (below graph)", result: "Move Left/Right"},
        ]
    };

